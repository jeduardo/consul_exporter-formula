[![pipeline status](https://gitlab.com/depositsolutions/consul_exporter-formula/badges/master/pipeline.svg)](https://gitlab.com/depositsolutions/consul_exporter-formula/commits/master)

# consul_exporter-formula

A saltstack formula created to setup [consul_exporter monitoring server](https://github.com/prometheus/consul_exporter).

# consul_exporter flags

Flags can be added into the pillar under the section ``options`` without the ``--``

```bash
./consul_exporter --help
```

* __`consul.allow_stale`:__ Allows any Consul server (non-leader) to service
    a read.
* __`consul.ca-file`:__ File path to a PEM-encoded certificate authority used to
    validate the authenticity of a server certificate.
* __`consul.cert-file`:__ File path to a PEM-encoded certificate used with the
    private key to verify the exporter's authenticity.
* __`consul.health-summary`:__ Collects information about each registered
    service and exports `consul_catalog_service_node_healthy`. This requires n+1
    Consul API queries to gather all information about each service. Health check
    information are available via `consul_health_service_status` as well, but
    only for services which have a health check configured. Defaults to true.
* __`consul.key-file`:__ File path to a PEM-encoded private key used with the
    certificate to verify the exporter's authenticity.
* __`consul.require_consistent`:__ Forces the read to be fully consistent.
* __`consul.server`:__ Address (host and port) of the Consul instance we should
    connect to. This could be a local agent (`localhost:8500`, for instance), or
    the address of a Consul server.
* __`consul.server-name`:__ When provided, this overrides the hostname for the
    TLS certificate. It can be used to ensure that the certificate name matches
    the hostname we declare.
* __`consul.timeout`:__ Timeout on HTTP requests to consul.
* __`log.format`:__ Set the log target and format. Example: `logger:syslog?appname=bob&local=7`
    or `logger:stdout?json=true`
* __`log.level`:__ Logging level. `info` by default.
* __`version`:__ Show application version.
* __`web.listen-address`:__ Address to listen on for web interface and telemetry.
* __`web.telemetry-path`:__ Path under which to expose metrics.


# pillar.example

```saltstack
consul_exporter:
  bin_dir: '/usr/bin'
  dist_dir: '/opt/consul_exporter/dist'
  version: '0.4.0'
  service: True
  service_user: 'consul_exporter'
  service_group: 'consul_exporter'
  consul_adress: 127.0.0.1:8500
  acl_token:
# options:
#   - 'version'

```

# Available states

## init

The essential consul_exporter state running both ``install``.

## install

- Download consul_exporter release from Github into the dist dir  (defaults to /opt/consul_exporter/dist).
- link the binary (defaults to /usr/bin/consul_exporter).
- Register the appropriate service definition with systemd.

This state can be called independently.

## uninstall

Remove the service, binaries, and configuration files. The data itself will be kept and needs to be removed manually, just to be on the safe side.

This state must always be called independently.

# Testing

## Prerequisites
The tests are using test kitchen with [inspec](https://www.inspec.io/downloads/) as verifier, so you need to have
- Ruby installed
- Docker installed

If you want to add tests you might want to take a look at the [documentation](http://testinfra.readthedocs.io/en/latest/modules.html#) for the modules.

## Running the test
If you run the tests for the first time you might need to run ``bundle install`` first. Afterwards you can run ``kitchen test ``.  
