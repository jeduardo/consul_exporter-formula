{% from slspath+"/map.jinja" import consul_exporter with context %}

consul_exporter-install-service:
  file.managed:
    - name: /etc/systemd/system/consul_exporter.service
    - source:  salt://{{ slspath }}/files/exporter.service.j2
    - template: jinja
    - context:
        consul_exporter: {{ consul_exporter }}

consul_exporter-service:
  service.running:
    - name: consul_exporter
    - enable: True
    - start: True
    - watch:
      - file: consul_exporter-install-service
      - file: consul_exporter-create-env

consul_exporter-create-env:
{% if consul_exporter.acl_token != None %}
  file.managed:
    - name: /etc/systemd/system/consul_exporter.service.d/env.conf
    - source: salt://{{ slspath }}/files/env.conf.j2
    - makedirs: True
    - template: jinja
    - context:
        consul_exporter: {{ consul_exporter }}
    - user: root
    - group: root
    - mode: 644
{% else %}
  file.absent:
    - name: /etc/systemd/system/consul_exporter.service.d
{% endif %}
